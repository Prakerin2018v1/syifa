<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
                <section class="page-section cta">
                  <div class="container">
                    <div class="row">
                      <div class="col-xl-9 mx-auto">
                        <div class="cta-inner text-center rounded">
                          <h2 class="section-heading mb-4">
                            <span class="section-heading-upper"></span>
                            <span class="section-heading-lower-text-bold">Selayang Pandang</span>
                          </h2>
                          <p class="mb-0">
Karakteristik pendidikan yang semakin cepat berkembang, mendorong kita senantiasa lebih berperan aktif dalam mentransformasikan pendidikan kepada generasi muslim dan muslimah agar lebih giat menuntut ilmu agama, pengetahuan dan teknologi.

SMK Al-Ittihad berbasis Pesantren mampu mensinergikan pendidikan umum dan pendidikan agama sebagai prioritas untuk mewujudkan siswa-siswi yang intelek, berakhlaq mulia dan berwawasan global.

SMK Al-Ittihad berdiri sejak tahun 2004 di bawah naungan Pondok Pesantren Al-Ittihad dan resmi mendapatkan izin operasional pada tahun 2006 berdasarkan SK Kepala Dinas Pendidikan Kabupaten Cianjur No. 421.1/712.A/DIKMEN/KAB/2006.

SMK Al-Ittihad menyelenggarakan dua program studi keahlian sebagai berikut:

Teknik Komputer dan Informatika
Kompetensi keahlian: Rekayasa Perangkat Lunak
Teknik Kimia
Kompetensi keahlian: Kimia Analisis
                              </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>

            <footer>
                @include('masterlayout.footer')
           </footer>    
    </body>
</html>