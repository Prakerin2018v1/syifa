<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
                <section class="page-section cta">
                  <div class="container">
                    <div class="row">
                      <div class="col-xl-9 mx-auto">
                        <div class="cta-inner text-center rounded">
                          <h2 class="section-heading mb-4">
                            <span class="section-heading-upper">JURUSAN</span>
                            <span class="section-heading-lower">Administrasi perkantoran</span>
                          </h2>
                          <p class="mb-0">
                          jurusan
                              </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>

            <footer>
                @include('masterlayout.footer')
           </footer>    
    </body>
</html>