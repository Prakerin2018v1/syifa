<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
                <section class="page-section cta">
                  <div class="container">
                    <div class="row">
                      <div class="col-xl-9 mx-auto">
                        <div class="cta-inner text-center rounded">
                          <h2 class="section-heading mb-4">
                          <span class="section-heading-lower">JURUSAN</span>
                            <span class="section-heading-upper">Rekayasa Perangkat Lunak</span>
                          </h2>
                          <p class="mb-0">
<li>Menerapkan teknik elektronika analog dan digital dasar</li>
<li>Menerapkan algoritma pemrograman tingkat dasar</li>
<li>Menerapkan algoritma tingkat lanjut</li>
<li>Membuat basis data</li>
<li>Menerapkan aplikasi basis data</li>
<li>Memahami pemrograman berbasis desktop</li>
<li>Membuat paket software aplikasi berbasis desktop</li>
<li>Mengoprasikan system oprasi jaringan computer</li>
<li>Menerapkan bahasa pemrograman SQL tingkat dasar</li>
<li>Menerapkan bahasa pemrograman SQL tingkat lanjut</li>
<li>Menerapkan dasar-dasar pembuatan web statis tingkat dasar</li>
                              </p>
                
                           <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="img/rpl.jpg" alt="">
                          </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>

            <footer>
                @include('masterlayout.footer')
           </footer>    
    </body>
</html>