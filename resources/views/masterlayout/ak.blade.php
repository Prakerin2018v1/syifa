<!DOCTYPE html>
<html>
    <head>
       @include('masterlayout.head')
    </head>    
    <body>
            <header>
                @include('masterlayout.header')
            </header>
        
            <div id="main">
                <section class="page-section cta">
                  <div class="container">
                    <div class="row">
                      <div class="col-xl-9 mx-auto">
                        <div class="cta-inner text-center rounded">
                          <h2 class="section-heading mb-4">
                            <span class="section-heading-lower">Analis Kimia</span>
                          </h2>
                          <p class="mb-0">
                          Melakukan percobaan dilaboratorium kimia
<li>Melakukan teknik dasar analisis kuantitatif</li>
<li>Mengelola laboratorium untuk analisis rutin</li>
<li>Merawat peralatan gelas, keramik dan alat penunjang kerja lainnya</li>
<li>Membuat laritan standard an pereaksi</li>
<li>Melakukan pengambilan sample</li>
<li>Melakukan sintesis senyawa kimia pada skala laboratorium</li>
<li>Melakukan analisis titrimetri</li>
<li>Melakukan analisis grafimetri</li>
<li>Melakukan analisis mikro biologi</li>
<li>Melakukan analisis kromatografi</li>
<li>Melakukan analisis jenis ( klasik)</li>
<li>Melakukan analisis bahan organic</li>
<li>Melakukan analisis air dan mineral</li>
<li>Melakukan analisis foto metri</li>
<li>Melakukan analisis elektro kimia</li>
                              </p>
                        </div>
                      </div>
                    </div>
                  </div>
                </section>
            </div>

            <footer>
                @include('masterlayout.footer')
           </footer>    
    </body>
</html>