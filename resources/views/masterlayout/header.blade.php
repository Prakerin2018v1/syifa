    <h1 class="site-heading text-center text-white d-none d-lg-block">
      <span class="site-heading-upper text-primary mb-3"></span>
      <span class="site-heading-lower text-primary">SMK AL-ITTIHAD</span>
    </h1>

    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark py-lg-4" id="mainNav">
      <div class="container">
        <a class="navbar-brand text-uppercase text-expanded font-weight-bold d-lg-none" href="#">Start Bootstrap</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav mx-auto">
            <li class="nav-item px-lg-4">
              <a class="nav-link text-uppercase text-primary" href="/">HOME
                <span class="sr-only">(current)</span>
              </a>
            </li>
            <li class="nav-item px-lg-4">
               <div class="dropdown">
               <a class="dropdown-toggele nav-link text-uppercase text-primary" data-toggle="dropdown">PROFIL</a>
              <div class="dropdown-menu">
                    <a class="dropdown-item" href="selayang">Selayang Pandang</a>
                    <a class="dropdown-item" href="struktu">Struktur</a>
                    <a class="dropdown-item" href="ap"></a>
                </div>
            </li>
            <li class="nav-item px-lg-4">
            <div class="dropdown">
               <a class="dropdown-toggele nav-link text-uppercase text-primary" data-toggle="dropdown">AKADEMIK</a>
               <div class="dropdown-menu">
                    <a class="dropdown-item" href="kegiatan">Kegiatan</a>
                    <a class="dropdown-item" href="mapel">Mata Pelajaran</a>
                     <a class="dropdown-item" href="mapel">Galery</a>
                </div>
            </div>
            <li class="nav-item px-lg-4">
            <div class="dropdown">
                <a class="dropdown-toggele nav-link text-uppercase text-primary" data-toggle="dropdown">JURUSAN</a>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="rpl">Rekayasa Perangkat Lunak</a>
                    <a class="dropdown-item" href="ak">Analis Kimia</a>
                    <a class="dropdown-item" href="ap">Administrasi Perkantoran</a>
                </div>
            </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>