@extends('frontend.index')
@section('content')
     <section class="page-section clearfix content">
      <div class="container">
        <div class="intro">
          <img class="intro-img img-fluid mb-3 mb-lg-0 rounded" src="img/kantor.jpg" alt="">
          <div class="intro-text left-0 text-center bg-faded p-5 rounded">
            <h2 class="section-heading mb-4">
              <span class="section-heading-upper">Visi &amp; Misi</span>
              <span class="section-heading-lower">SMK ALTIE</span>
            </h2>
            <p class="text-body">
            VISI SMK   AL-ITTIHAD
“Membentuk warga SMK yang Religius, Unggul dan Inovatif”
‘ALIMAN, SHOLIHAN, MUJAHIDAN”

            </p>
            <div class="intro-button mx-auto">
              <a class="btn btn-warning btn-xl" href="#">Visit Us Today!</a>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section class="page-section cta">
      <div class="container">
        <div class="row">
          <div class="col-xl-9 mx-auto">
            <div class="cta-inner text-center rounded">
              <h2 class="section-heading mb-4">
                <span class="section-heading-upper">Sejarah Singkat</span>
                <span class="section-heading-lower">Pesantren</span>
              </h2>
              <p class="mb-0">SEJARAH AL-ITTIHAD DAN SMK AL-ITTIHAD

Tahun 1997 bagi Bapak H. Acep Badruddin, BA (saudagar sukses di Jakarta) merupakan tahun pencerahan bathin, sebab disamping sukses dalam menjalani kehidupan di Jakarta yang penuh tantangan itu, juga terinspirasi dengan kesuksesannya mengelola Yayasan Budi Mulya di Jakarta yang bergerak dibidang pendidikan formal dan informal (RA, TKA, TPA, TPA, MDA). Kesuksesan beliau dalam dunia bisnis dan pergaulan di tengah masyarakat itu, membuat beliau berfikir tentang tanah wakaf mertuanya H. Mahfud yang berlokasi di Rawabango Karangtengah Cianjur. Mau diapakan tanah wakaf tersebut?

Setelah lama merenung, Pak H. Acep Badruddin yang beristrikan (Almh) Hj. Mimin Rukmini itu, kemudian memutuskan (ber’azam) untuk mendirikan Pondok Pesantren. Salah satu pertimbangannya adalah karena beliau memiliki anggota keluarga (menantu) yang mahir dibidang pendidikan pesantren, bernama KH. Kamali Abd.Ghani yang menikah dengan putrinya yang ketiga Dra.Hj.Ety Muflihah.

Maksud dan ‘azam Pak H.Acep Badruddin itu, kemudian diutarakan kepada mantunya tersebut (KH. Kamali) dengan penuh keseriusan dan terkesan berhati-hati (khawatir mantunya kurang berkenan, sebab kondisi dan posisinya di Jakarta saat sedang populer karena ilmu dan ketawadluannya).
                </p>
            </div>
          </div>
        </div>
      </div>
    </section>
        @endsection
