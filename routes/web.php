<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return View::make('masterlayout.content');
});

Route::get('about', function() {
  return View::make('masterlayout.about');  
});

Route::get('selayang', function() {
  return View::make('masterlayout.selayang');  
});

Route::get('kegiatan', function() {
  return View::make('masterlayout.kegiatan');  
});

Route::get('mapel', function() {
  return View::make('masterlayout.mapel');  
});

Route::get('rpl', function() {
  return View::make('masterlayout.rpl');  
});

Route::get('ak', function() {
  return View::make('masterlayout.ak');  
});

Route::get('ap', function() {
  return View::make('masterlayout.ap');  
});